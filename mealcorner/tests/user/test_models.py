import pytest

from tests.user.factories import UserFactory, PersonalInfoFactory
from user.models import User, PersonalInfo


class TestUserModel:

    @pytest.mark.unit
    def test_employee_assertion(self):
        user = UserFactory.build(user_type=User.EMPLOYEE)
        assert user.user_type == User.EMPLOYEE

    @pytest.mark.unit
    def test_restaurant_assertion(self):
        user = UserFactory.build(user_type=User.RESTAURANT)
        assert user.user_type == User.RESTAURANT


class TestPersonalInfoModel:

    @pytest.mark.django_db
    def test_personal_info_user(self):
        user = UserFactory()
        info = PersonalInfoFactory(
            user=user,
            first_name=user.first_name,
            last_name=user.last_name
        )

        assert info.user.id == user.id
        assert info.first_name == user.first_name
        assert info.last_name == user.last_name