from datetime import date
from django.conf import settings

from user.models import User
from tests.user.factories import UserFactory

import os
import json
import pytest

url_prefix = "/api/v1"


class TestUserAuthEndpoints:
    endpoint = f"{url_prefix}/rest-auth/"

    @pytest.mark.django_db
    def test_login_superadmin(self, api_client, get_superadmin_token):
        login_creds = {
            "email": os.environ.get('SUPERADMIN_EMAIL'),
            "password": os.environ.get('SUPERADMIN_PASS'),
        }

        res = api_client.post(
            self.endpoint + 'login/',
            data=login_creds,
            format='json',
        )
        # print(res.request)

        assert res.status_code == 200

        res_data = json.loads(res.content)
        assert res_data['key'] == get_superadmin_token.key


    @pytest.mark.django_db
    def test_create_employee(self, api_client, get_superadmin_token, get_test_pass):
        emp_user = UserFactory.build(user_type=User.EMPLOYEE)
        emp_pass = get_test_pass
        reg_creds = {
            "email": emp_user.email,
            "full_name": emp_user.first_name + ' ' + emp_user.last_name,
            "password1": emp_pass,
            "password2": emp_pass,
        }

        token_key = f"Token {get_superadmin_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.post(
            self.endpoint + 'registration/',
            data=reg_creds,
            format='json',
        )
        # print(res.request)

        assert res.status_code == 201

        res_data = json.loads(res.content)

        assert 'key' in res_data
        assert type(res_data['key']) == str


    @pytest.mark.django_db
    def test_login_employee(self, api_client, get_employee_user, get_test_pass):
        emp_user = get_employee_user
        emp_pass = get_test_pass

        login_creds = {
            "email": emp_user.email,
            "password": emp_pass,
        }

        res = api_client.post(
            self.endpoint + 'login/',
            data=login_creds,
            format='json',
        )
        # print(res.request)

        assert res.status_code == 200

        res_data = json.loads(res.content)

        assert 'key' in res_data
        assert type(res_data['key']) == str


    @pytest.mark.django_db
    def test_logout(self, api_client, get_superadmin_token, get_employee_token):

        token_key = f"Token {get_superadmin_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.post(
            self.endpoint + 'logout/',
            data=None
        )
        # print(res.request)

        assert res.status_code == 200