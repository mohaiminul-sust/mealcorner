
from user.models import User, PersonalInfo

import factory

class UserFactory(factory.django.DjangoModelFactory):
    first_name = factory.faker.Faker('first_name')
    last_name = factory.faker.Faker('last_name')
    email = factory.faker.Faker('email')

    class Meta:
        model = User


class PersonalInfoFactory(factory.django.DjangoModelFactory):
    first_name = factory.faker.Faker('first_name')
    last_name = factory.faker.Faker('last_name')

    class Meta:
        model = PersonalInfo