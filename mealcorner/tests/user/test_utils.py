import pytest
from django.conf import settings

from user.utils import get_formatted_name, get_parts_from_name


@pytest.mark.unit
def test_get_formatted_name():
    name = 'TESTNAME'
    f_name = get_formatted_name(name)

    assert f_name == name.title()