import pytest, factory

from user.serializers import UserSerializer, PersonalInfoSerializer
from tests.user.factories import UserFactory, PersonalInfoFactory


class TestUserSerializer:

    @pytest.mark.unit
    def test_serialize_model(self):
        user = UserFactory.build()
        serializer = UserSerializer(user)

        assert serializer.data

    @pytest.mark.django_db
    def test_serialize_data(self):
        valid_serializer_data = factory.build(
            dict,
            FACTORY_CLASS=UserFactory
        )

        serializer = UserSerializer(data=valid_serializer_data)

        assert serializer.is_valid()
        assert serializer.errors == {}


class TestPersonalInfoSerializer:

    @pytest.mark.django_db
    def test_serialize_model(self):
        info = PersonalInfoFactory(user=UserFactory())
        serializer = PersonalInfoSerializer(info)

        assert serializer.data

    @pytest.mark.django_db
    def test_serialize_data(self):
        valid_serializer_data = factory.build(
            dict,
            FACTORY_CLASS=PersonalInfoFactory
        )

        serializer = PersonalInfoSerializer(data=valid_serializer_data)

        assert serializer.is_valid()
        assert serializer.errors == {}