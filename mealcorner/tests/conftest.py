import pytest

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from django.core.management import call_command
from random import randint
from user.models import User

import os
import dramatiq

TEST_USER_PASS = 'restpass'

@pytest.fixture
def get_test_pass():
    return TEST_USER_PASS


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def get_superadmin_token():
    call_command('create_su')
    return Token.objects.get(user__username=os.environ.get('SUPERADMIN_EMAIL'))


def get_employee_user_instance():
    return User.objects.create_employee_user(
        email=f"test_employee{randint(1, 9)}@mealcorner.com",
        password=TEST_USER_PASS
    )


@pytest.fixture
def get_employee_user():
    return get_employee_user_instance()


@pytest.fixture
def get_employee_token():
    emp_user = get_employee_user_instance()
    token, _ = Token.objects.get_or_create(user=emp_user)
    return token


@pytest.fixture
def broker():
    broker = dramatiq.get_broker()
    broker.flush_all()
    return broker


@pytest.fixture
def worker(broker):
    worker = dramatiq.Worker(broker, worker_timeout=100)
    worker.start()
    yield worker
    worker.stop()