import pytest, factory
from core.serializers import RestaurantSerializer, RestaurantMenuSerializer
from tests.core.factories import RestaurantFactory, RestaurantMenuFactory
from datetime import date


class TestRestaurantSerializer:

    @pytest.mark.unit
    def test_serialize_model(self):
        restaurant = RestaurantFactory.build()
        serializer = RestaurantSerializer(restaurant)

        assert serializer.data


    @pytest.mark.django_db
    def test_serialize_data(self):
        valid_serializer_data = factory.build(
            dict,
            FACTORY_CLASS=RestaurantFactory
        )

        serializer = RestaurantSerializer(data=valid_serializer_data)

        assert serializer.is_valid()
        assert serializer.errors == {}


class TestRestaurantMenuSerializer:

    @pytest.mark.django_db
    def test_serialize_model(self):
        menu = RestaurantMenuFactory(restaurant=RestaurantFactory(), order_date=date.today())
        serializer = RestaurantMenuSerializer(menu)

        assert serializer.data


    @pytest.mark.django_db
    def test_serialize_data(self):
        valid_serializer_data = factory.build(
            dict,
            FACTORY_CLASS=RestaurantMenuFactory
        )

        serializer = RestaurantMenuSerializer(data=valid_serializer_data)

        assert not serializer.is_valid()
        assert serializer.errors != {}