import pytest

from tests.core.factories import RestaurantFactory, RestaurantMenuFactory
from datetime import date


class TestRestaurantModel:

    @pytest.mark.django_db
    def test_increment_vote_count(self):
        restaurant = RestaurantFactory()
        assert restaurant.total_votes == 0

        restaurant.increment_total_votes()
        assert restaurant.total_votes == 1

    @pytest.mark.django_db
    def test_decrement_vote_count(self):
        restaurant = RestaurantFactory(total_votes=7)
        assert restaurant.total_votes == 7

        restaurant.decrement_total_votes()
        assert restaurant.total_votes == 6

    @pytest.mark.django_db
    def test_decrement_vote_count_on_zero(self):
        restaurant = RestaurantFactory()
        assert restaurant.total_votes == 0

        restaurant.decrement_total_votes()
        assert restaurant.total_votes == 0


class TestRestaurantMenuModel:
    today = date.today()

    @pytest.mark.django_db
    def test_update_vote_count(self, get_employee_user):
        menu = RestaurantMenuFactory(restaurant=RestaurantFactory(), order_date=self.today)
        assert menu.total_votes == 0

        menu.voted_by.add(get_employee_user)
        menu.update_vote_count()
        assert menu.total_votes == 1

    @pytest.mark.django_db
    def test_add_vote(self, get_employee_user):
        menu = RestaurantMenuFactory(restaurant=RestaurantFactory(), order_date=self.today)
        assert menu.total_votes == 0

        user = get_employee_user

        menu.add_vote(user)
        assert menu.voted_by.filter(id=user.id).exists()
        assert menu.total_votes == 1

    @pytest.mark.django_db
    def test_remove_vote(self, get_employee_user):
        menu = RestaurantMenuFactory(restaurant=RestaurantFactory(), order_date=self.today)
        assert menu.total_votes == 0

        user = get_employee_user

        menu.add_vote(user)
        assert menu.voted_by.filter(id=user.id).exists()
        assert menu.total_votes == 1

        menu.remove_vote(user)
        assert not menu.voted_by.filter(id=user.id).exists()
        assert menu.total_votes == 0