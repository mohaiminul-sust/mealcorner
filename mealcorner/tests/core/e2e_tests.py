from datetime import date
from django.conf import settings

from user.models import User
from tests.core.factories import RestaurantFactory, RestaurantMenuFactory

import os
import json
import pytest

url_prefix = "/api/v1"


class TestRestaurantEndpoints:
    endpoint = f"{url_prefix}/restaurants/"

    @pytest.mark.django_db
    def test_create(self, api_client, get_superadmin_token, get_test_pass):
        restaurant = RestaurantFactory.build()
        expected_payload = {
            "name": restaurant.name,
            "email": restaurant.email,
            "descripion": restaurant.description,
            "address": restaurant.address
        }

        post_data = expected_payload.copy()
        post_data['password'] = get_test_pass

        token_key = f"Token {get_superadmin_token.key}"
        # print(token_key, post_data)
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.post(
            self.endpoint,
            data=post_data,
            format='json',
        )
        # print(res.request)

        assert res.status_code == 201

        created_res = json.loads(res.content)
        assert restaurant.name == created_res['name']
        assert restaurant.email == created_res['email']
        assert restaurant.address == created_res['address']



    @pytest.mark.django_db
    def test_get_by_id(self, api_client, get_superadmin_token):
        restaurant = RestaurantFactory()

        token_key = f"Token {get_superadmin_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.get(
            self.endpoint + f'{restaurant.id}/',
        )
        print(res.request)

        assert res.status_code == 200

        res_data = json.loads(res.content)
        # print("res id" + restaurant.id)
        assert str(restaurant.id) == res_data['id']


    @pytest.mark.django_db
    def test_update(self, api_client, get_superadmin_token):
        old_restaurant = RestaurantFactory()
        new_restaurant = RestaurantFactory.build()
        payload = {
            "address": new_restaurant.address
        }
        token_key = f"Token {get_superadmin_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.patch(
            self.endpoint + f'{old_restaurant.id}/',
            data=payload,
            format='json',
        )
        # print(res.request)

        assert res.status_code == 200

        updated_res = json.loads(res.content)
        assert new_restaurant.address == updated_res['address']


    @pytest.mark.django_db
    def test_create_menu_for_restaurant(self, api_client, get_superadmin_token):
        restaurant = RestaurantFactory()
        menu = RestaurantMenuFactory.build()
        expected_payload = {
            "menu_text": menu.menu_text,
        }

        post_data = expected_payload.copy()
        post_data['restaurant'] = f"{restaurant.id}"

        token_key = f"Token {get_superadmin_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.post(
            self.endpoint + f'{restaurant.id}/create-menu/',
            data=post_data,
            format='json',
        )
        # print(res.request)

        assert res.status_code == 201

        created_menu = json.loads(res.content)
        assert menu.menu_text == created_menu['menu_text']
        assert restaurant.email == created_menu['restaurant']['email']
        assert restaurant.name == created_menu['restaurant']['name']


class TestRestaurantMenuEndpoints:
    endpoint = f"{url_prefix}/restaurant-menus/"

    @pytest.mark.django_db
    def test_get_by_id(self, api_client, get_superadmin_token):
        menu = RestaurantMenuFactory(
            restaurant=RestaurantFactory(),
            order_date=date.today()
        )

        token_key = f"Token {get_superadmin_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.get(
            self.endpoint + f'{menu.id}/',
        )
        # print(res.request)

        assert res.status_code == 200

        res_data = json.loads(res.content)
        assert str(menu.id) == res_data['id']


    @pytest.mark.django_db
    def test_update(self, api_client, get_superadmin_token):
        menu = RestaurantMenuFactory(
            restaurant=RestaurantFactory(),
            order_date=date.today()
        )

        payload = {
            "menu_text": "Lorem Ipsum"
        }

        token_key = f"Token {get_superadmin_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.patch(
            self.endpoint + f'{menu.id}/',
            data=payload,
            format='json',
        )
        # print(res.request)

        assert res.status_code == 200

        updated_res = json.loads(res.content)
        assert payload['menu_text'] == updated_res['menu_text']


    @pytest.mark.django_db
    def test_todays_menu_list(self, api_client, get_superadmin_token):
        menu1 = RestaurantMenuFactory(
            restaurant=RestaurantFactory(),
            order_date=date.today()
        )
        menu2 = RestaurantMenuFactory(
            restaurant=RestaurantFactory(),
            order_date=date.today()
        )

        token_key = f"Token {get_superadmin_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.get(
            self.endpoint + 'today/',
        )
        # print(res.request)

        assert res.status_code == 200

        res_data = json.loads(res.content)
        assert res_data['count'] == 2
        assert len(res_data['results']) == 2


    @pytest.mark.django_db
    def test_vote_menu_for_today_with_suser(self, api_client, get_superadmin_token, get_employee_token):
        menu = RestaurantMenuFactory(
            restaurant=RestaurantFactory(),
            order_date=date.today()
        )

        token_key = f"Token {get_superadmin_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.post(
            self.endpoint + f'{menu.id}/vote-for-today/',
            data=None
        )
        # print(res.request)

        assert res.status_code == 200

        res_data = json.loads(res.content)
        match_res = f"Voted {menu.restaurant.name}'s Menu on {menu.order_date}"
        assert res_data['detail'] == match_res


    @pytest.mark.django_db
    def test_vote_menu_for_today_with_empuser(self, api_client, get_employee_token):
        menu = RestaurantMenuFactory(
            restaurant=RestaurantFactory(),
            order_date=date.today()
        )

        token_key = f"Token {get_employee_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.post(
            self.endpoint + f'{menu.id}/vote-for-today/',
            data=None
        )
        # print(res.request)

        assert res.status_code == 200

        res_data = json.loads(res.content)
        match_res = f"Voted {menu.restaurant.name}'s Menu on {menu.order_date}"
        assert res_data['detail'] == match_res



    @pytest.mark.django_db
    def test_winner_for_today(self, api_client, get_employee_token):

        # simutate voting scenario
        menu = RestaurantMenuFactory(
            restaurant=RestaurantFactory(),
            order_date=date.today()
        )

        token_key = f"Token {get_employee_token.key}"
        api_client.credentials(HTTP_AUTHORIZATION=token_key)

        res = api_client.post(
            self.endpoint + f'{menu.id}/vote-for-today/',
            data=None
        )
        # print(res.request)

        assert res.status_code == 200

        # assert winner response
        res = api_client.get(self.endpoint + 'winner/')

        assert res.status_code == 200

        res_data = json.loads(res.content)
        assert res_data['total_menus'] == 1
        assert res_data['winner_menu']['id'] == str(menu.id)