import pytest
from django.conf import settings

from core.utils import get_work_days


@pytest.mark.unit
def test_get_work_days():
    c_work_day = list(set(settings.ALL_DAYS) - set(settings.OFF_DAYS))
    work_days = get_work_days()

    assert work_days == c_work_day