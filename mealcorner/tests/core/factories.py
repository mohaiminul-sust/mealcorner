
from core.models import Restaurant, RestaurantMenu

import factory

class RestaurantFactory(factory.django.DjangoModelFactory):
    name = factory.faker.Faker('company')
    address = factory.faker.Faker('address')
    description = factory.faker.Faker('catch_phrase')
    email = factory.faker.Faker('email')

    class Meta:
        model = Restaurant


class RestaurantMenuFactory(factory.django.DjangoModelFactory):
    menu_text = factory.faker.Faker('paragraph')

    class Meta:
        model = RestaurantMenu
