import pytest
from tests.core.factories import RestaurantFactory, RestaurantMenuFactory
from core.services import get_winner_restaurant_menu, update_winner_of_the_day
from core.models import WinnerRecord

from datetime import date


@pytest.mark.django_db
def test_get_winner_service(get_employee_user):
    today = date.today()

    menu1 = RestaurantMenuFactory(
        restaurant=RestaurantFactory(),
        order_date=today,
    )
    menu2 = RestaurantMenuFactory(
        restaurant=RestaurantFactory(),
        order_date=today,
    )

    user = get_employee_user
    menu1.voted_by.add(user)

    winner, others = get_winner_restaurant_menu(query_date=today)
    assert str(winner.id) == str(menu1.id)
    assert str(others[0].id) == str(menu2.id)


@pytest.mark.django_db
def test_update_wod_state(get_employee_user):
    today = date.today()

    menu1 = RestaurantMenuFactory(
        restaurant=RestaurantFactory(),
        order_date=today,
    )
    menu2 = RestaurantMenuFactory(
        restaurant=RestaurantFactory(),
        order_date=today,
    )
    user = get_employee_user

    assert WinnerRecord.objects.count() == 0

    menu1.voted_by.add(user)
    update_winner_of_the_day(today)

    assert WinnerRecord.objects.count() > 0

    record = WinnerRecord.objects.first()

    assert str(record.restaurant_menu.id) == str(menu1.id)
    assert str(record.restaurant.id) == str(menu1.restaurant.id)
    assert record.total_votes == 1

    # assert str(record.restaurant_menu.id) == str(menu1.id)
