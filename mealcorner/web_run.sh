#!/bin/sh

python manage.py collectstatic --no-input

# Uncomment following lines to automate migration on up
#
python manage.py makemigrations
python manage.py migrate --no-input
python manage.py create_su


# Uncomment following line to automate test on up
#
pytest --disable-warnings -v

# Run server
#
python manage.py runserver_plus 0.0.0.0:8000

exec "$@"