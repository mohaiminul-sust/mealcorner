#!/bin/sh

python manage.py collectstatic --no-input

# Uncomment following lines to automate migration on up
#
python manage.py migrate --no-input
python manage.py create_su


# Uncomment following line to automate test on up
#
# pytest --disable-warnings -v

# Bind server app with gunicorn
#
gunicorn -t 300 -b 0.0.0.0:8000 mealcorner.wsgi:application

exec "$@"