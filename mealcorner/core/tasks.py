from datetime import date
import logging, dramatiq, time

from .services import update_winner_of_the_day

logger = logging.getLogger(__name__)


# for testing dramatiq threadworks
#
@dramatiq.actor
def count_backwards():
    words = list()
    for i in range(100):
        logger.info(f"Counting {i}")
        words.append(i)
        time.sleep(1)

    print(f"Done Job : Count {len(words)}")


@dramatiq.actor
def sync_winner_of_the_day(order_date_iso: str):
    order_date = date.fromisoformat(order_date_iso)
    update_winner_of_the_day(order_date)