from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from django.utils.http import urlencode
from django.utils.crypto import get_random_string
from django.conf import settings
from django.core.paginator import Paginator

from rest_framework.authtoken.models import Token
from rest_framework import generics, status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable, ValidationError
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import LimitOffsetPagination

from rest_framework_extensions.cache.mixins import CacheResponseMixin

from datetime import date, datetime
from user.models import User

from .models import Restaurant, RestaurantMenu, WinnerRecord
from .serializers import RestaurantSerializer, RestaurantMenuSerializer, WinnerHistorySerializer, RestaurantMenuCreateSerializer
from .utils import IsSuperUser, IsRestaurantUser, IsEmployeeUser
from .services import get_winner_restaurant_menu
from .tasks import sync_winner_of_the_day

import dramatiq, logging

logger = logging.getLogger(__name__)


class RestaurantCreateView(generics.CreateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsSuperUser, )
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer

    def create(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if 'password' not in request.data:
            raise ValidationError('Password is required')

        try:
            user = User.objects.create_restaurant_user(request.data['email'], request.data['password'])

        except Exception as ex:
            raise ValidationError(str(ex))

        token, _ = Token.objects.get_or_create(user=user)

        restaurent = serializer.save()

        restaurent.owner = user
        restaurent.save()

        restaurant_data = self.get_serializer(restaurent, many=False).data
        return Response(restaurant_data, status=201)


class RestaurantRetrieveUpdateView(generics.RetrieveUpdateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, )
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer

    def patch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            # raise PermissionDenied()
            if request.user.is_employee():
                raise PermissionDenied()

            elif request.user.is_restaurant_user():
                if restaurant.owner != request.user:
                    raise PermissionDenied()

        return self.partial_update(request, args, kwargs)



class RestaurantMenuCreateView(generics.CreateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsRestaurantUser | IsSuperUser, )
    queryset = RestaurantMenu.objects.all()
    serializer_class = RestaurantMenuCreateSerializer

    def create(self, request, *args, **kwargs):
        try:
            restaurant = Restaurant.objects.get(id=kwargs['pk'])
        except Restaurant.DoesNotExist:
            raise NotFound('Restaurant not found by given id')

        if RestaurantMenu.objects.filter(restaurant=restaurant, order_date=date.today()).exists():
            raise ValidationError('Can not create another menu today.')

        if request.user.is_restaurant_user():
            if restaurant.owner != request.user:
                raise PermissionDenied()

        serializer = self.get_serializer(data=request.data, context={
            'restaurant': restaurant
        })
        serializer.is_valid(raise_exception=True)
        menu = serializer.save()
        res_data = RestaurantMenuSerializer(menu, many=False).data

        return Response(res_data, status=201)


class RestaurantMenuRetrieveUpdateView(generics.RetrieveUpdateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, )
    queryset = RestaurantMenu.objects.all()
    serializer_class = RestaurantMenuSerializer

    def patch(self, request, *args, **kwargs):
        if request.user.is_employee() and not request.user.is_superuser:
            raise PermissionDenied()
        elif request.user.is_restaurant_user():
            if restaurant.owner != request.user:
                raise PermissionDenied()

        return self.partial_update(request, args, kwargs)


class TodaysMenuView(CacheResponseMixin, generics.ListAPIView, LimitOffsetPagination):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, )
    queryset = RestaurantMenu.objects.filter(order_date=date.today())
    serializer_class = RestaurantMenuSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())

        paginated = self.paginate_queryset(queryset)
        data = self.get_serializer_class()(paginated, many=True).data
        return self.get_paginated_response(data)


class VoteMenuView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsEmployeeUser | IsSuperUser, )

    def check_voting_permission(self, restaurant_menu: RestaurantMenu, voter: User):
        today_date = date.today()

        if restaurant_menu.order_date != today_date:
            raise PermissionDenied('Can\'t vote past or future menus')

        # TODO: remove other votes if already voted today
        if RestaurantMenu.objects.filter(order_date=today_date, voted_by__id=voter.id).exists():
            raise PermissionDenied("Already Voted restaurant today")

        return


    def post(self, request, *args, **kwargs):
        try:
            restaurant_menu = RestaurantMenu.objects.get(id=kwargs['pk'])

        except RestaurantMenu.DoesNotExist:
            raise NotFound('Restaurant Menu not found by given id')

        voter = request.user
        self.check_voting_permission(restaurant_menu, voter)

        # add vote
        restaurant_menu.add_vote(voter)
        restaurant = restaurant_menu.restaurant
        # update wod service call
        # update_winner_of_the_day()
        task_ref = sync_winner_of_the_day.send(date.today().isoformat())
        logger.info("Fired task : sync_winner_of_the_day")
        logger.info(task_ref)


        return Response({
            "detail": f"Voted {restaurant.name}'s Menu on {restaurant_menu.order_date}"
        })


class WinnerOfTheDayView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        winner, others = get_winner_restaurant_menu(date.today())
        winner_data = RestaurantMenuSerializer(winner, many=False).data

        others = RestaurantMenuSerializer.with_eager_load(others)
        others_data = RestaurantMenuSerializer(others, many=True).data

        return Response({
            "total_menus": others.count() + 1 if winner else 0,
            "winner_menu": winner_data,
            "other_menus": others_data
        })


class WinnerHistoryView(generics.ListAPIView, LimitOffsetPagination):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, )
    serializer_class = WinnerHistorySerializer
    queryset = WinnerRecord.objects.order_by('-order_date')

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        paginated = self.paginate_queryset(queryset)
        data = self.get_serializer_class()(paginated, many=True).data
        return self.get_paginated_response(data)