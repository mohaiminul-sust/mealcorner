from django.urls import path

from .api_views import RestaurantCreateView, RestaurantMenuCreateView, TodaysMenuView, VoteMenuView, RestaurantRetrieveUpdateView, RestaurantMenuRetrieveUpdateView, WinnerOfTheDayView, WinnerHistoryView


urlpatterns = [
    path('restaurants/', RestaurantCreateView.as_view(), name='restaurant_create_view'),

    path('restaurants/<uuid:pk>/', RestaurantRetrieveUpdateView.as_view(), name='restaurant_ret_upd_view'),

    path('restaurants/<uuid:pk>/create-menu/', RestaurantMenuCreateView.as_view(), name='restaurant_menu_create_view'),

    path('restaurant-menus/today/', TodaysMenuView.as_view(), name='restaurant_menu_today_list_view'),

    path('restaurant-menus/<uuid:pk>/', RestaurantMenuRetrieveUpdateView.as_view(), name='restaurant_menu_rest_upd_view'),

    path('restaurant-menus/<uuid:pk>/vote-for-today/', VoteMenuView.as_view(), name='restaurant_menu_vote_view'),

    path('restaurant-menus/winner/', WinnerOfTheDayView.as_view(), name='restaurant_menu_winner_view'),


    path('restaurants/winner-history/', WinnerHistoryView.as_view(), name='restaurant_menu_winner_history_view'),

]
