from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.authtoken.models import Token

from django.conf import settings
from django.utils.translation import gettext as _
from datetime import date, datetime

from .models import Restaurant, RestaurantMenu, WinnerRecord
from user.models import User
from user.serializers import UserSerializer


class RestaurantSerializer(serializers.ModelSerializer):
    owner = UserSerializer(required=False, read_only=True)
    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'email', 'description', 'address', 'owner', )
        extra_kwargs = {
            'email': {
                'required': True
            }
        }


class MenuRestaurantSerializer(serializers.ModelSerializer):
    owner = UserSerializer(required=False, read_only=True)

    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'email', 'owner', 'address', )


class RestaurantMenuCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RestaurantMenu
        fields = ('menu_text', 'menu_image', 'menu_file', )
        extra_kwargs = {
            'menu_image': {
                'required': False
            },
            'menu_file': {
                'required': False
            },
        }

    def validate(self, data):
        data['restaurant'] = self.context['restaurant']
        data['order_date'] = date.today()
        return data


class RestaurantMenuSerializer(serializers.ModelSerializer):
    restaurant = MenuRestaurantSerializer(read_only=True)

    class Meta:
        model = RestaurantMenu
        exclude = ('slug', 'updated_at', 'voted_by', 'is_draft', )


    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('restaurant')
        return queryset


class WinnerHistorySerializer(serializers.ModelSerializer):
    restaurant_name = serializers.CharField(read_only=True, source='restaurant.name')
    restaurant_menu_id = serializers.CharField(read_only=True, source='restaurant_menu.id')

    class Meta:
        model = WinnerRecord
        fields = ('order_date', 'restaurant_name', 'restaurant_menu_id', 'total_votes', 'updated_at')
