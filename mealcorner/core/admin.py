from django.contrib import admin
from user.models import User
from .models import Restaurant, RestaurantMenu, WinnerRecord

class RestaurantMenuInline(admin.TabularInline):
    model = RestaurantMenu
    fields = ('order_date', 'id', 'total_votes', )
    readonly_fields = fields
    can_delete = False
    extra = 0
    show_change_link = True

    def total_votes(self, obj) -> int:
        return obj.voted_by.count()

    def has_add_permission(self, request, obj=None):
        return False


class RestaurantAdmin(admin.ModelAdmin):
    # date_hierarchy = '-created_at'
    inlines = (RestaurantMenuInline, )
    list_display = ('name', 'id', 'owner', 'is_draft', 'total_votes', 'slug', 'created_at', )
    list_select_related = ('owner', )
    readonly_fields = ('slug', 'total_votes',)
    search_fields = ('name', 'email', 'owner__email', )
    list_per_page = 30

    def get_inlines(self, request, obj=None):
        return self.inlines if obj else []

admin.site.register(Restaurant, RestaurantAdmin)


class RestaurantMenuAdmin(admin.ModelAdmin):
    list_display = ('id', 'order_date', 'restaurant', 'is_draft', 'total_votes', 'created_at', )
    list_select_related = ('restaurant', )
    readonly_fields = ('workday', 'slug', 'total_votes', )
    search_fields = ('restaurant__name', 'restaurant__email', )
    list_per_page = 30

    def total_votes(self, obj) -> int:
        return obj.voted_by.count()

admin.site.register(RestaurantMenu, RestaurantMenuAdmin)


class WinnerRecordAdmin(admin.ModelAdmin):
    list_display = ('order_date', 'restaurant', 'restaurant_menu', 'total_votes', 'created_at',)
    list_select_related = ('restaurant', )
    readonly_fields = list_display
    search_fields = ('restaurant__name', 'restaurant__email', )
    list_per_page = 30

    def has_add_permission(self, request, obj=None):
        return False

admin.site.register(WinnerRecord, WinnerRecordAdmin)
