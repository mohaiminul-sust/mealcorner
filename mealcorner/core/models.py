from django.db import models

from datetime import datetime, timedelta
import calendar

from django.utils import timezone
from django.utils.translation import gettext as _
from django.utils.text import slugify
from django.utils.html import mark_safe
from django.contrib.postgres.fields import JSONField

from django.conf import settings
from django.conf.urls.static import static

from ckeditor.fields import RichTextField

from .utils import UUIDModel, PublishableModel


class Restaurant(PublishableModel):
    name = models.CharField(max_length=200, unique=True)

    owner = models.ForeignKey('user.User', on_delete=models.SET_NULL, null=True, blank=True, help_text="Select owner for this restaurant", related_name='owner_restaurant')

    email = models.EmailField(_("Restaurant mail address"), max_length=254, null=True, blank=True)

    description = models.TextField(blank=True, help_text="Enter a brief description of the restaurant (optional)")

    address = models.TextField(blank=True, help_text="Enter restaurant address (optional)")

    slug = models.SlugField(max_length=300, null=True, blank=True)

    total_votes = models.PositiveIntegerField(_("Total Votes"), default=0)

    class Meta:
        verbose_name_plural = 'Restaurants'
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Restaurant, self).save(*args, **kwargs)

    def increment_total_votes(self):
        self.total_votes += 1
        self.save(update_fields=['total_votes', ])

    def decrement_total_votes(self):
        if self.total_votes == 0:
            return

        self.total_votes -= 1
        self.save(update_fields=['total_votes', ])


class RestaurantMenu(PublishableModel):

    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, null=False, blank=False, help_text="Select restaurant for this menu", related_name="menu_restaurnt")

    menu_text = RichTextField(blank=True, help_text="Enter the menu")

    menu_image = models.ImageField(null=True, blank=True, upload_to='menu_images/')

    menu_file = models.FileField(null=True, blank=True, upload_to='menu_files/')

    order_date = models.DateField(_("Date"), auto_now=False, auto_now_add=False)

    workday = models.CharField(_("Day"), max_length=50, blank=True, help_text="auto generated on save")

    slug = models.SlugField(max_length=300, null=True, blank=True)

    voted_by = models.ManyToManyField('user.User', verbose_name=_("Voters"), blank=True)

    total_votes = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name_plural = 'Restaurant Menus'
        ordering = ['-created_at']
        unique_together = ('restaurant', 'order_date', )

    def __str__(self):
        return self.slug

    def save(self, *args, **kwargs):
        if self.order_date:
            self.workday = calendar.day_name[self.order_date.weekday()]
            slug_text = f"{str(self.order_date)}__{self.workday}"
            self.slug = slugify(slug_text)

        super(RestaurantMenu, self).save(*args, **kwargs)

    def update_vote_count(self):
        c_vote_count = self.voted_by.count()
        if self.total_votes == c_vote_count:
            return

        self.total_votes = c_vote_count
        self.save(update_fields=['total_votes', ])

    def add_vote(self, voter):
        self.voted_by.add(voter)
        self.update_vote_count()
        self.restaurant.increment_total_votes()

    def remove_vote(self, voter):
        self.voted_by.remove(voter)
        self.update_vote_count()
        self.restaurant.decrement_total_votes()


class WinnerRecord(UUIDModel):

    order_date = models.DateField(_("Order Date"), auto_now=False, auto_now_add=False, unique=True)

    restaurant = models.ForeignKey(Restaurant, on_delete=models.SET_NULL, null=True, blank=True, related_name="record_restaurnt")

    restaurant_menu = models.ForeignKey(RestaurantMenu, on_delete=models.SET_NULL, null=True, blank=True, related_name="record_menu")

    total_votes = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return str(self.order_date)

    def update_record(self, winner: RestaurantMenu):
        self.restaurant_menu=winner
        self.restaurant=winner.restaurant
        self.total_votes=winner.total_votes
        self.save()