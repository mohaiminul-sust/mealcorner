from django.core.management.base import BaseCommand
from django.conf import settings

from rest_framework.authtoken.models import Token
from user.models import User

import os


class Command(BaseCommand):
    def handle(self, *args, **options):
        print("Searching SuperUser...")
        user = User.objects.filter(is_superuser=True).first()

        if not user:
            user = User.objects.create_superuser(
                email=os.environ.get('SUPERADMIN_EMAIL'),
                password=os.environ.get('SUPERADMIN_PASS'),
            )
            print("Created SU")

        else:
            print("Fetching Already Created SU")

        print(f"User Mail : {user.email}")
        print(f"User Password : {os.environ.get('SUPERADMIN_PASS')}")

        token, _ = Token.objects.get_or_create(user=user)
        print(f"Token Key : {token.key}")
