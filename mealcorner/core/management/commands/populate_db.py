from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.conf import settings

from rest_framework.authtoken.models import Token
from mealcorner.core.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        call_command('create_su')