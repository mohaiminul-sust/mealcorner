from datetime import date, timedelta
from django.db.models import Count, Q

from .models import RestaurantMenu, WinnerRecord

import logging, dramatiq

logger = logging.getLogger(__name__)


def get_winner_restaurant_menu(query_date: date, exclude_menus=list()):
    res = RestaurantMenu.objects.filter(order_date=query_date)
    if len(exclude_menus):
        res = res.exclude(id__in=exclude_menus)

    res = res.order_by('-total_votes')

    winner = res.first()

    if not winner:
        return None, RestaurantMenu.objects.none()

    others = res.exclude(id=winner.id).order_by('-total_votes')

    # check for continuation of three days
    yesterday = query_date - timedelta(days=1)
    tdb = query_date - timedelta(days=2)

    if WinnerRecord.objects.filter(order_date=yesterday, restaurant_menu=winner).exists() and \
        WinnerRecord.objects.filter(order_date=tdb, restaurant_menu=winner).exists():
        return get_winner_restaurant_menu(query_date, exclude_menus=exclude_menus.append(winner.id))

    return winner, others


def update_winner_of_the_day(query_date: date):
    winner, _ = get_winner_restaurant_menu(query_date=query_date)
    if not winner:
        logger.info("No Winner Yet")
        return

    current_record = WinnerRecord.objects.filter(order_date=query_date).first()

    if not current_record:
        WinnerRecord.objects.create(
            restaurant_menu=winner,
            restaurant=winner.restaurant,
            order_date=query_date,
            total_votes=winner.voted_by.count(),
        )
        logger.info("Winner Record Created")

    else:
        if current_record.restaurant_menu == winner:
            current_record.total_votes = winner.voted_by.count()
            current_record.save(update_fields=['total_votes', ])
            logger.info(f"Updated total votes to {current_record.total_votes} for {current_record.restaurant_menu}")

        else:
            if current_record.total_votes < winner.voted_by.count():
                logger.info(f"Swipping Winner from {current_record.restaurant.name} to {winner.restaurant.name}")
                current_record.update_record(winner)