from django.conf import settings
from django.db import models
from django.apps import apps

from rest_framework.views import exception_handler, APIView
from rest_framework.exceptions import APIException
from rest_framework.permissions import BasePermission
from rest_framework.pagination import PageNumberPagination as RDFPagination
from rest_framework.settings import api_settings
from django.core.files.base import ContentFile
from io import BytesIO
import hashlib
import hmac
import uuid
import requests
import json
import os
import random
import string
import traceback


class UUIDModel(models.Model):
    """ UUIDModel
    An abstract base class model that sets the primary to a UUID field.
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class BlameableUUIDModel(UUIDModel):
    """ BlameableUUIDModel
    An abstract base class model that extends UUIDModel and adds is_draft field.
    """
    created_by = models.ForeignKey('user.User', on_delete=models.SET_NULL, null=True, blank=True, help_text="Select creator for this entry")

    class Meta:
        abstract= True


class PublishableModel(UUIDModel):
    """ PublishableModel
    An abstract base class model that extends UUIDModel and adds is_draft field.
    """
    is_draft = models.BooleanField(default=False, help_text="Controls published status")

    class Meta:
        abstract = True


class IsAppUser(BasePermission):
    """
    Allows access only to logged in and signed up client app users.
    """

    def has_permission(self, request, view):
        return request.user and not request.user.is_staff \
               and hasattr(request.user, 'profile')


class IsEmployeeUser(BasePermission):
    """
    Allows access only to logged in and signed up employee users with proper attrs.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_employee()


class IsRestaurantUser(BasePermission):
    """
    Allows access only to logged in and signed up restaurant users with proper attrs.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_restaurant_user()


class IsSuperUser(BasePermission):
    """
    Allows access only to logged in and signed up super users
    """
    def has_permission(self, request, view):
        return request.user and request.user.is_superuser


def get_work_days() -> list:
    return list(set(settings.ALL_DAYS) - set(settings.OFF_DAYS))