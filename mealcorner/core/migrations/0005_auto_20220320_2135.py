# Generated by Django 3.2.5 on 2022-03-20 15:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_alter_restaurantmenu_voted_by'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurantmenu',
            name='menu_file',
            field=models.FileField(blank=True, null=True, upload_to='menu_files/'),
        ),
        migrations.AddField(
            model_name='restaurantmenu',
            name='menu_image',
            field=models.ImageField(blank=True, null=True, upload_to='menu_images/'),
        ),
    ]
