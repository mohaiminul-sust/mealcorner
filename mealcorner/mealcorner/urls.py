"""mealcorner URL Configuration"""

from django.conf import settings
from django.conf.urls import handler404, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, reverse_lazy
from django.views.generic.base import RedirectView

from rest_framework.documentation import include_docs_urls


admin.site.site_header = "MealCorner"
admin.site.site_title = "MealCorner"
admin.site.index_title = "Dashboard"

doc_prefernces = {
    'title': 'MealCorner API',
    'description': 'API Documentation for Client Integration',
    'public': True
}

handler404 = 'user.views.error_404_view'

preamble = 'api/v1/'

urlpatterns = [
    url(r'^$', RedirectView.as_view(url=reverse_lazy('admin:index'))),
    path('admin/', include('smuggler.urls')),
    path('admin/', admin.site.urls),

    # API Paths
    path(f'{preamble}', include('user.urls')),
    path(f'{preamble}', include('core.urls')),

    # Doc Path
    path(r'api-docs/', include_docs_urls(**doc_prefernces)),
]

if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)