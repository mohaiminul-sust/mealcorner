from allauth.account.signals import user_signed_up, password_changed, email_confirmed
# from allauth.socialaccount.signals import social_account_added
from django.contrib.auth.signals import user_logged_in
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save
from datetime import datetime
from django.utils.timezone import now

from .models import User
from .utils import get_current_domain
from .tasks import sync_personal_info_on_user_save


# model signals (hooks)
@receiver(post_save, sender=User)
def populate_user_extras(sender, instance=None, created=False, **kwargs):
    sync_personal_info_on_user_save.send(str(instance.id), created)


# admin login signal
@receiver(user_logged_in)
def update_last_login_time(sender, user, request, **kwargs):
    user.last_login = now()
    user.save()