import dramatiq
from django.db import transaction

from rest_framework.authtoken.models import Token

from .models import User, PersonalInfo

import logging

logger = logging.getLogger(__name__)


@dramatiq.actor
def create_user_api_token(user_id: str):
    user = User.objects.filter(id=user_id).first()
    if not user:
        return

    with transaction.atomic():
        token, _ = Token.objects.get_or_create(user=user)
        logger.info(f"Token Created: {token.key}")


@dramatiq.actor
def sync_personal_info_on_user_save(user_id: str, created: bool):
    user = User.objects.filter(id=user_id).first()
    if not user:
        return

    with transaction.atomic():
        if created:
            create_user_api_token.send(user_id)

            PersonalInfo.objects.create(user=user, first_name=user.first_name, last_name=user.last_name)
            logger.info("PersonalInfo Created")

        else:
            try:
                info = PersonalInfo.objects.get(user=user)
                info.first_name = user.first_name
                info.last_name = user.last_name
                info.save(update_fields=['first_name', 'last_name', ])

                logger.info("PersonalInfo Updated")

            except PersonalInfo.DoesNotExist:
                PersonalInfo.objects.get_or_create(
                    user=user,
                    first_name=user.first_name,
                    last_name=user.last_name
                )
                logger.info("PersonalInfo Created")