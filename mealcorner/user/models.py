from django.db import models
from django.db.models import Sum
from uuid import uuid4
from django.contrib.auth.models import AbstractUser

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.utils.timezone import now
from django.utils.translation import gettext as _
from django.core.validators import RegexValidator
from django.conf import settings

from .user_manager import UserManager


class User(AbstractUser):
    EMPLOYEE = "EMP"
    RESTAURANT = "RES"
    USER_TYPE = (
        (EMPLOYEE, "EMPLOYEE"),
        (RESTAURANT, "RESTAURANT"),
    )

    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    email = models.EmailField(max_length=254, unique=True)
    avatar = models.ImageField(upload_to='user_avatars', null=True, blank=True)
    user_type = models.CharField(max_length=3, choices=USER_TYPE, default=EMPLOYEE)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def __str__(self):
        return self.email if self.email.strip() != '' else str(self.id)

    def assigned_group(self) -> str:
        return self.groups.values_list('name', flat = True)[0] if self.groups.exists() else '-'

    def is_employee(self) -> bool:
        return self.user_type == self.EMPLOYEE

    def is_restaurant_user(self) -> bool:
        return self.user_type == self.RESTAURANT


class PersonalInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name='personal_info')

    first_name = models.CharField(max_length=150, blank=True, null=True, help_text="First Name")

    last_name = models.CharField(max_length=150, blank=True, null=True, help_text="Last Name")

    date_of_birth = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True, help_text="Birthday")

    contact_number = models.CharField(max_length=17, null=True, blank=True, help_text="Phone Number", default="")

    address = models.TextField(null=True, blank=True, help_text="Address")

    created_at = models.DateField(auto_now=False, auto_now_add=True)

    updated_at = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Personal Infos"

    def __str__(self):
        return self.user.email if self.user.email != '' else str(self.user.id)

    def calculated_age(self):
        return relativedelta(date.today(), self.date_of_birth).years

    def full_name(self) -> str:
        return self.first_name + " " + self.last_name if not self.first_name == '' or not self.last_name == '' else ''