from django.urls import path, include
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from allauth.account.views import ConfirmEmailView

from .api_views import RestLoginView, RestRegisterView, RestVerifyEmailView, null_view, complete_view, PersonalInfoViewSet, EmployeeListView, RestaurantUserListView


urlpatterns = [
    url(r'^', include('django.contrib.auth.urls')),

    # custom auth overrides
    url(r'^rest-auth/registration/$', RestRegisterView.as_view(), name='rest_signup'),
    url(r'^rest-auth/registration/verify-email/$', RestVerifyEmailView.as_view(), name='rest_email_verify'),
    url(r'^rest-auth/login/$', RestLoginView.as_view(), name='rest_login'),
    url(r'^registration/account-email-verification-sent/', null_view, name='account_email_verification_sent'),
    url(r'^registration/account-confirm-email/(?P<key>[-:\w]+)/$', ConfirmEmailView.as_view(), name='account_confirm_email'),
    url(r'^registration/complete/$', complete_view, name='account_confirm_complete'),
    url(r'^password-reset/confirm/<uidb64>/<token>/', null_view, name='password_reset_confirm'),

    # rest-auth urls
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),


    #all auth
    url(r'^account/', include('allauth.urls')),

    #info model
    url(r'^personal-info/', PersonalInfoViewSet.as_view(), name='personal_info_views'),

    path('users/employee/', EmployeeListView.as_view(), name='employee_list_view'),
    path('users/restaurant/', RestaurantUserListView.as_view(), name='restaurant_user_list_view'),
]
