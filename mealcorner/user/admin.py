from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
# from django.contrib.auth.models import Permission
# from django.conf import settings

from .models import User, PersonalInfo

# for debugging permission objects (e.g. deleting leftovers from unused/deleted models)
# admin.site.register(Permission)

class PersonalInfoInline(admin.StackedInline):
    model = PersonalInfo
    can_delete = False

class UserAdmin(UserAdmin):
    date_hierarchy = 'date_joined'
    list_display = ('email', 'id', 'user_type', 'staff_status', 'last_login', 'date_joined')
    list_filter = ('user_type', 'is_staff', )
    search_fields = ('email',)

    # def account_type(self, obj):
    #     return obj.assigned_group()

    def staff_status(self, obj):
        return obj.is_staff
    staff_status.boolean = True

admin.site.register(User, UserAdmin)


class PersonalInfoAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'
    list_display = ('user', 'first_name', 'last_name', 'date_of_birth', 'contact_number', )
    search_fields = ('user__email', 'contact_number', 'first_name', 'last_name', )
    list_filter = ('user__groups', )
    ordering = ('-created_at', )

    def has_add_permission(self, request, obj=None):
        return True


admin.site.register(PersonalInfo, PersonalInfoAdmin)