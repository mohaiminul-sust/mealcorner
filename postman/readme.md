# Postman Collection Files for API Documentation and Testing
For live API Docs visit [here](https://documenter.getpostman.com/view/198290/UVsSLiEv)

## Run via [Postman](https://www.postman.com/downloads/)

- From postman folder, import [collection](./TestSuite.postman_collection.json) and [environment](./Dev.postman_environment.json) files into postman
- Run the collection from postman (covered with tests)