version: '3.7'

services:
  db:
    image: postgres:12.0-alpine
    volumes:
      - postgres_data:/var/lib/postgresql/data/
    env_file:
      - ./.env.staging.db
    networks:
      - main

  web: &web
    build:
        context: ./mealcorner
        dockerfile: Dockerfile.prod
    container_name: mealcorner-web
    restart: unless-stopped
    command: gunicorn -t 300 -b 0.0.0.0:8000 mealcorner.wsgi:application
    volumes:
      - static_volume:/home/mealcorner/web/static
      - media_volume:/home/mealcorner/web/uploads
    expose:
      - 8000
    env_file:
      - ./.env.staging
    depends_on:
      - db
    networks:
      - main

  asgiserver:
    <<: *web
    container_name: mealcorner-asgiserver
    build:
      context: ./mealcorner
      dockerfile: Dockerfile.prod
      args:
        DJANGO_ENV: development
    command: daphne mealcorner.asgi:application --bind 0.0.0.0 --port 9000
    ports: []

  dramatiq:
    <<: *web
    container_name: mealcorner-dramatiq
    command: python manage.py rundramatiq
    volumes:
      - ./mealcorner/:/usr/src/mealcorner/
    ports: []
    depends_on:
      - redis

  nginx-proxy:
    container_name: mealcorner-nginx-proxy
    build: ./nginx
    restart: always
    ports:
      - 443:443
      - 80:80
    volumes:
      - static_volume:/home/mealcorner/web/staticfiles
      - media_volume:/home/mealcorner/web/mediafiles
      - certs:/etc/nginx/certs
      - html:/usr/share/nginx/html
      - vhost:/etc/nginx/vhost.d
      - /var/run/docker.sock:/tmp/docker.sock:ro
    depends_on:
      - web

  nginx-proxy-letsencrypt:
    image: jrcs/letsencrypt-nginx-proxy-companion
    env_file:
      - ./.env.staging.proxy-companion
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - certs:/etc/nginx/certs
      - html:/usr/share/nginx/html
      - vhost:/etc/nginx/vhost.d
      - mealcorner:/etc/mealcorner.sh
    depends_on:
      - nginx-proxy

  pgadmin:
    container_name: mealcorner-pgadmin
    image: dpage/pgadmin4
    environment:
      PGADMIN_DEFAULT_EMAIL: pgadmin4@pgadmin.org
      PGADMIN_DEFAULT_PASSWORD: admin
    volumes:
      - pgadmin_data:/root/.pgadmin
    ports:
      - "5050:80"
    depends_on:
      - db
    networks:
      - main
    restart: unless-stopped

  redis:
    image: redis:6.0.5
    hostname: redis
    container_name: mealcorner-redis
    volumes:
      - redis_data:/data
    ports:
      - "6379"
    env_file: ./.env.dev
    networks:
      - main

  redis-commander:
    container_name: mealcorner-redis-commander
    hostname: redis-commander
    image: rediscommander/redis-commander:latest
    restart: always
    environment:
      - REDIS_HOST=redis
    ports:
      - "8085:8081"
    depends_on:
      - "redis"
    networks:
      - main

networks:
  main:

volumes:
  postgres_data:
  redis_data:
  pgadmin_data:
  static_volume:
  media_volume:
  certs:
  html:
  vhost:
  mealcorner: