# MealCorner Server

## What is it?
### API Server for CornerCase Python Task

- [Project Dependencies](mealcorner/requirements.txt)
- [Docker Compose File](docker-compose.yml)
- [Dockerfile for django project](mealcorner/Dockerfile)

## Getting Started

```bash
$ docker-compose build
$ docker-compose up -d

# On first run, Use the main web service's log to get genetrated superadmin's credentials
$ docker-compose logs | grep 'mealcorner-web'
```

## Run tests ([pytest](https://docs.pytest.org/en/7.1.x/))
Run the e2e and unit tests using pytest and docker

```bash
$ docker exec -it mealcorner-web pytest --disable-warnings -v
```

## Initial Setup

The following process has been automated in application start up process using shell scripts and docker in v1.0.6

You can bypass the automation by commenting out the following lines in [web_run.sh](./mealcorner/web_run.sh)

```bash
python manage.py makemigrations
python manage.py migrate --no-input
python manage.py create_su
```

Then you can trigger them using docker like below as needed

```bash
# You need to run the migrations for initial setup ...
$ docker exec -it mealcorner-web python manage.py makemigrations # to trigger third party migration hooks
$ docker exec -it mealcorner-web python manage.py migrate

# You need to create superuser for initial setup
$ docker exec -it mealcorner-web python manage.py create_su
# use the credentials at .env files
```

You can also create a superuser with custom input credentials by executing createsuperuser command provided by django
```bash
$ docker exec -it mealcorner-web python manage.py createsuperuser

```
Login to the [admin panel](http://localhost:8000/) to check system logs and data

##  Explore API Documentation ([Postman](https://www.postman.com/downloads/))
For API documentation visit [here](https://documenter.getpostman.com/view/198290/UVsSLiEv)


### For running tests or exploring locally
- From postman folder, import [collection](postman/TestSuite.postman_collection.json) and [environment](postman/Dev.postman_environment.json) files into postman app
- Run the collection from postman app (covered with postman tests)
